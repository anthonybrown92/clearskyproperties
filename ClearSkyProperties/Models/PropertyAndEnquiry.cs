﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClearSkyProperties.Models
{
    public class PropertyAndEnquiry
    {
        public Property Property { get; set; }

        public Enquiry Enquiry { get; set; }
    }


    public class Enquiry
    {
        public int ID { get; set; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Msg { get; set; }
    }
    public class Property
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Enquiries { get; set; }
        public string Desc { get; set; }
        public string Img { get; set; }
    }
}
