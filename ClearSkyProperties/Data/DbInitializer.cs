﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClearSkyProperties.Models;

namespace ClearSkyProperties.Data
{
    public class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Properties.Any())
            {
                Console.WriteLine("Properties already seeded");
                return;   // DB has been seeded
            }

            var properties = new Property[]
            {
            new Property{Name="1 Test Street",Price=35000099, Enquiries=0, Desc="Beautiful Property on 1 Test Street... blah blah... blah... Beautiful Property on 1 Test Street... blah blah... blah... Beautiful Property on 1 Test Street... blah blah... blah... Beautiful Property on 1 Test Street... blah blah... blah...", Img="Content/property1.jpg"},
            new Property{Name="2 Test Street",Price=30000099, Enquiries=0, Desc="Great Property on 2 Test Street... blah blah... blah... Great Property on 2 Test Street... blah blah... blah... Great Property on 2 Test Street... blah blah... blah... Great Property on 2 Test Street... blah blah... blah...", Img="Content/property2.jpg"},
            new Property{Name="3 Test Street",Price=34000095, Enquiries=0, Desc="Spacious Property on 3 Test Street... blah blah... blah... Spacious Property on 3 Test Street... blah blah... blah... Spacious Property on 3 Test Street... blah blah... blah... Spacious Property on 3 Test Street... blah blah... blah... Spacious Property on 3 Test Street... blah blah... blah...", Img="Content/property3.jpg"},
            new Property{Name="4 Test Street",Price=32000095, Enquiries=0, Desc="Nice Property on 4 Test Street... blah blah... blah... Nice Property on 4 Test Street... blah blah... blah... Nice Property on 4 Test Street... blah blah... blah... Nice Property on 4 Test Street... blah blah... blah... Nice Property on 4 Test Street... blah blah... blah...", Img="Content/property4.jpg"},
            new Property{Name="5 Test Street",Price=29000095, Enquiries=0, Desc="Beautiful Property on 5 Test Street... blah blah... blah... Beautiful Property on 5 Test Street... blah blah... blah...Beautiful Property on 5 Test Street... blah blah... blah... Beautiful Property on 5 Test Street... blah blah... blah... Beautiful Property on 5 Test Street... blah blah... blah...", Img="Content/property5.jpg"},
            new Property{Name="6 Test Street",Price=28000090, Enquiries=0, Desc="Terrible Property on 6 Test Street... blah blah... blah... Terrible Property on 6 Test Street... blah blah... blah... Terrible Property on 6 Test Street... blah blah... blah... Terrible Property on 6 Test Street... blah blah... blah... Terrible Property on 6 Test Street... blah blah... blah...", Img="Content/property6.jpg"},
            new Property{Name="7 Test Street",Price=28500090, Enquiries=0, Desc="Moldy Property on 7 Test Street... blah blah... blah... Moldy Property on 7 Test Street... blah blah... blah... Moldy Property on 7 Test Street... blah blah... blah... Moldy Property on 7 Test Street... blah blah... blah... Moldy Property on 7 Test Street... blah blah... blah...", Img="Content/property7.jpg"},
            new Property{Name="8 Test Street",Price=32000090, Enquiries=0, Desc="Student Property on 8 Test Street... blah blah... blah... Student Property on 8 Test Street... blah blah... blah... Student Property on 8 Test Street... blah blah... blah... Student Property on 8 Test Street... blah blah... blah... Student Property on 8 Test Street... blah blah... blah...", Img="Content/property8.jpg"},
            new Property{Name="9 Test Street",Price=31000089, Enquiries=0, Desc="Beautiful Property on 9 Test Street... blah blah... blah... Beautiful Property on 9 Test Street... blah blah... blah... Beautiful Property on 9 Test Street... blah blah... blah... Beautiful Property on 9 Test Street... blah blah... blah...", Img="Content/property9.jpg"},
            new Property{Name="10 Test Street",Price=30000089, Enquiries=0, Desc="Beautiful Property on 10 Test Street... blah blah... blah... Beautiful Property on 10 Test Street... blah blah... blah... Beautiful Property on 10 Test Street... blah blah... blah... Beautiful Property on 10 Test Street... blah blah... blah...", Img="Content/property10.jpg"},
            };
            foreach (Property p in properties)
            {
                context.Properties.Add(p);
            }
            context.SaveChanges();

        }
    }
}
