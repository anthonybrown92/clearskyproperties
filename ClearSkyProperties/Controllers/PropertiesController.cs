﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClearSkyProperties.Data;
using ClearSkyProperties.Models;
using Microsoft.AspNetCore.Authorization;

namespace ClearSkyProperties.Controllers
{
    public class PropertiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PropertiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Properties
        public async Task<IActionResult> Index()
        {
            return View(await _context.Properties.ToListAsync());
        }

        // GET: Properties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Properties
                .FirstOrDefaultAsync(m => m.ID == id);
            if (@property == null)
            {
                return NotFound();
            }

            return View(@property);
        }

        public async Task<IActionResult> Enquire(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Properties.FindAsync(id);
            if (@property == null)
            {
                return NotFound();
            }
            PropertyAndEnquiry propertyAndEnquiry = new PropertyAndEnquiry();
            propertyAndEnquiry.Property = @property;
            return PartialView("Enquire", propertyAndEnquiry);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEnquiry([Bind("ID,PropertyID,PropertyName,FirstName,LastName,PhoneNumber,Email,Msg")] Enquiry @enquiry)
        {
            if (ModelState.IsValid)
            {
                //Find the Property related and increment the 'Enquiries' column
                Property property = new Property { ID = enquiry.PropertyID };
                _context.Properties.Attach(property).Property(x => x.Enquiries).CurrentValue += 1;
                //Add the Enquiry to the Enquiries table.
                _context.Add(@enquiry);
                await _context.SaveChangesAsync();
                TempData["alertMessage"] = "Your enquiry has been succesfully received.";
                return RedirectToAction(nameof(Index));
            }
            return View(@enquiry);
        }


        private bool PropertyExists(int id)
        {
            return _context.Properties.Any(e => e.ID == id);
        }
    }
}
