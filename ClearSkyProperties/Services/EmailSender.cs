﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using ClearSkyProperties.Services;
using System.Collections.Generic;
using System;

namespace ClearSkyProperties.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } 

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(System.Environment.GetEnvironmentVariable("SendGridKey"), "Clear Sky Properties: Confirm your Email Address.", message, email);
        }

        public Task Execute(string apiKey, string subject, string message, string email)
        {
            Console.WriteLine("apiKey: " + apiKey);
            var client = new SendGridClient(apiKey);
            Console.WriteLine("client: " + client);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("anthonybrown2002@hotmail.com", "Clear Sky Properties"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message,
            };
            msg.AddTo(new EmailAddress(email));
            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}

